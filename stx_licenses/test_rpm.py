#
# SPDX-License-Identifier: Apache-2.0
#

import pytest
from rpm import RPM
import excepts

class TestsRpms:

    deps = [("/bin/bash", "/bin/bash"),
            ("/bin/sh", "/bin/sh"),
            ("/usr/bin/db_stat", "/usr/bin/db_stat"),
            ("config(rpm) = 4.14.2.1-2.fc28", "config"),
            ("coreutils", "coreutils"),
            ("curl", "curl"),
            ("libacl.so.1()(64bit)", "libacl.so.1"),
            ("libarchive.so.13()(64bit)", "libarchive.so.13"),
            ("libbz2.so.1()(64bit)", "libbz2.so.1"),
            ("libc.so.6()(64bit)", "libc.so.6"),
            ("libc.so.6(GLIBC_2.2.5)(64bit)", "libc.so.6"),
            ("libc.so.6(GLIBC_2.3.4)(64bit)", "libc.so.6"),
            ("libc.so.6(GLIBC_2.4)(64bit)", "libc.so.6"),
            ("libcap.so.2()(64bit)", "libcap.so.2"),
            ("libcrypto.so.1.1()(64bit)", "libcrypto.so.1.1"),
            ("libdb-5.3.so()(64bit)", "libdb-5.3.so"),
            ("libdl.so.2()(64bit)", "libdl.so.2"),
            ("libelf.so.1()(64bit)", "libelf.so.1"),
            ("liblua-5.3.so()(64bit)", "liblua-5.3.so"),
            ("liblzma.so.5()(64bit)", "liblzma.so.5"),
            ("libm.so.6()(64bit)", "libm.so.6"),
            ("libpopt.so.0()(64bit)", "libpopt.so.0"),
            ("popt(x86-64) >= 1.10.2.1", "popt"),
            ("rpmlib(CompressedFileNames) <= 3.0.4-1", "rpmlib")
        ]


    def test_create_rpm_object(self):
        # Test rely on be run in a rpm environment.
        rpm = RPM("rpm")
        assert rpm.name == "rpm"
        rpm.get_dependencies()
        assert len(rpm.deps) > 0
        assert rpm.license == "GPLv2+"
        assert len(rpm.version) > 0

    def test_create_with_invalid_rpm(self):
        with pytest.raises(excepts.RPMNotFound) as e:
            rpm = RPM("badrpm")
        assert str(e.value) == "badrpm not found"

    def test_find_library_ldconfig(self):
        lib = "libc.so.6"
        rpm = RPM("rpm")
        assert rpm._find_library_path(lib) == "/lib/libc.so.6" \
            or rpm._find_library_path(lib) == "/lib64/libc.so.6"
        with pytest.raises(excepts.LibraryNotFound) as e:
            rpm._find_library_path("Some bad library")
        assert rpm.find_library(lib).startswith('glibc')


    @pytest.mark.parametrize("input, expected", deps)
    def test_extract_library_from_output(self, input, expected):
        rpm = RPM("rpm")
        assert expected == rpm._sanitize_dependency(input)

