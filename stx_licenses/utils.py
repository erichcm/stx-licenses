#
# SPDX-License-Identifier: Apache-2.0
#

try:
    import configparser
except:
    import ConfigParser as configparser
import tempfile
import time
import threading
import subprocess
from io import StringIO

import excepts

class Komander(object):
    """ Launch commands and returns the output """

    @staticmethod
    def run(cmd, timeout=0):
        """
        Executes a command in the shell,
        Returns a ShellCommand object with the results
        """
        tmp_stdout = tempfile.SpooledTemporaryFile()
        tmp_stderr = tempfile.SpooledTemporaryFile()

        command = ShellCommand(cmd)
        p = subprocess.Popen(command.cmd, stdout=tmp_stdout,
                             stderr=tmp_stderr, shell=True)

        # Wait for subprocess to complete.

        if timeout > 0:
            while p.poll() is None and timeout > 0:
                time.sleep(0.25)
                timeout -= 0.25
            p.kill()

        p.communicate()
        tmp_stdout.seek(0)
        tmp_stderr.seek(0)

        command.stdout = tmp_stdout.read()
        command.stderr = tmp_stderr.read()
        command.retcode = p.returncode

        tmp_stdout.close()
        tmp_stderr.close()

        return command

class ShellCommand(object):
    """ Represents a shell command """

    def __init__(self, cmd=""):
        self.cmd = cmd
        self.stdout = ""
        self.stderr = ""
        self.retcode = ""

    def __str__(self):
        msg = "Command : {0}\nstdout = {1}\nstderr = {2}\nretcode = {3}"
        return msg.format(self.cmd,
                          self.stdout,
                          self.stderr,
                          self.retcode)

class StarlingXInfo:
    """ To retrieve information of the running system."""

    BUILD_INFO_FILE = "/etc/build.info"

    def __init__(self):
        self.os = None
        self.version = None
        self.build_target = None
        self.build_type = None
        self.build_id = None
        self.job = None
        self.build_by = None
        self.build_number = None
        self.build_host = None
        self.build_date = None


    def is_starlingx(self):
        """ Checks if this a StarlingX system by looking for /etc/build.info"""
        try:
            f = open(self.BUILD_INFO_FILE)
        except IOError:
            return False
        return True

    def read_info(self):
        """ Read values from /etc/build.info """
        if not self.is_starlingx():
            raise excepts.MissingStarlingXInfo("build.info not found.")

        with open(self.BUILD_INFO_FILE, 'r') as f:
            config_str = u"[dummy]\n" + f.read()

        buf = StringIO(config_str)
        config = configparser.ConfigParser()
        # FIXME: readfp will be deprecated, however it's not available
        # in StarlingX's configparser module.
        config.readfp(buf)
        self.get_values_from_config(config)

    def get_values_from_config(self, cfg):
        def getter(cfg, key):
            return cfg.get('dummy', key).replace('"', '')

        self.os = getter(cfg, 'OS')
        self.version = getter(cfg, 'SW_VERSION')
        self.build_target = getter(cfg, 'BUILD_TARGET')
        self.build_type = getter(cfg, 'BUILD_TYPE')
        self.build_id = getter(cfg, 'BUILD_ID')
        self.job = getter(cfg, "JOB")
        self.build_by = getter(cfg, 'BUILD_BY')
        self.build_number = getter(cfg, 'BUILD_NUMBER')
        self.build_host = getter(cfg, 'BUILD_HOST')
        self.build_date = getter(cfg, 'BUILD_DATE')
