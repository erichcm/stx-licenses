#
# SPDX-License-Identifier: Apache-2.0
#

from utils import Komander
import excepts

class RPM:
    """ To represent a RPM """

    def __init__(self, name):
        """ Gets a rpm name and retrieve information of the system."""
        self.name = name
        self.deps = list()
        self.version = None
        self.license = None
        self.check_rpm_installed()
        self.get_info_from_rpm()

    def check_rpm_installed(self):
        cmd = "rpm -qi {}".format(self.name)
        res = Komander.run(cmd)
        if res.retcode != 0:
            # Sometimes the requirements lists packages that can be found
            # only by the --whatprovides flag, For example if a package
            # requires python2-prettytable this one won't be found by a
            # simple rpm -qi command as the installed one is python-prettytable
            # Therefore a second parse is needed to ensure we get the correct
            # package name.
            cmd = "rpm -q --whatprovides {} | grep -v i686".format(self.name)
            res = Komander.run(cmd)
            if res.retcode != 0 or b"no package provides" in res.stdout:
                raise excepts.RPMNotFound("{} not found".format(self.name))
            self.name = self.remove_duplicate(res.stdout.strip())


    def get_info_from_rpm(self):
        cmd = "rpm -q --queryformat " \
            "'%{{name}}|%{{version}}|%{{license}}\\n' {}".format(self.name)
        res = Komander.run(cmd)
        output = self.remove_duplicate(res.stdout)
        s = output.rstrip().split('|')
        if len(s) != 3: # Received an invalid list.
            raise excepts.RPMInvalidInfo("{} has invalid info".format(self.name))
        else:
            self.name = s[0]
            self.version = s[1]
            self.license = s[2]

    def remove_duplicate(self, res):
        return res.decode().split('\n')[0]

    def has_dep(self, dep):
        for d in self.deps:
            if dep == d.name:
                return True
        return False

    def get_dependencies(self):
        cmd = "rpm -q --queryformat '[%{{requires}},]\\n' {}".format(self.name)
        res = Komander.run(cmd)
        s = res.stdout.rstrip().decode().split(',')
        for dep in s:
            clean_dep = self._sanitize_dependency(dep)

            if len(clean_dep) > 0 \
               and clean_dep != 'rpmlib' \
               and '/' not in clean_dep \
               and clean_dep != 'config' \
               and clean_dep != 'rtld':
                # FIXME: Some dependencies, like rubygem are listed but
                # those doesn't exist as a package. This causes an exception.
                # The workaround is just not add those dependencies as are
                # invalid anyway.
                # Another case if libceph-common.so.0. which is not handled by
                # ldconfig, possibly a bug in that rpm.
                try:
                    pkg_dep = self.library_to_rpm(clean_dep)
                    if not self.has_dep(pkg_dep):
                        self.deps.append(RPM(pkg_dep))
                except:
                    pass

    def library_to_rpm(self, lib):
        if ".so" in lib:
            return self.find_library(lib)
        return lib

    def _sanitize_dependency(self, dep):
        idx = dep.find("(")
        if idx >= 0:
            return dep[:idx]
        else:
            return dep

    def _find_library_path(self, library):
        cmd = "ldconfig -p | grep {}".format(library)
        res = Komander.run(cmd)
        if res.retcode != 0:
            raise excepts.LibraryNotFound("{} library not found".format(library))

        libpath = res.stdout.rstrip().decode().split('=>')[-1]
        return libpath.strip()


    def find_library(self, library):
        libpath = self._find_library_path(library)
        cmd = "rpm -q --whatprovides --queryformat '%{{name}}' {}".format(libpath)
        res = Komander.run(cmd)
        if res.retcode != 0:
            msg = "{} library package not found".format(library)
            raise excepts.LibraryPackageNotFound(cmd)
        return res.stdout.strip().decode()

