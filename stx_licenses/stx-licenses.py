from __future__ import print_function
#
# SPDX-License-Identifier: Apache-2.0
#

import sys
from utils import StarlingXInfo, Komander
from excepts import *
from rpm import RPM

def main():
    try:
        stx = StarlingXInfo()
        stx.read_info()
    except MissingStarlingXInfo as e:
        print("failure: {}".format(e))
        print("This program needs to be run in a StarlingX system.")
        return 1

    cmd = "rpm -qa | grep .tis."
    res = Komander.run(cmd)
    if res.retcode != 0:
        print("Error getting StarlingX packages: {}".format(res.stderr))
        return 1
    pkgs_list = res.stdout.split('\n')
    
    pkgs = []
    for p in pkgs_list:
        if p != '':
            rpm = RPM(p)
            rpm.get_dependencies()
            for line in rpm.deps:
                print("{},{},{},{},{}".format(rpm.name,
                                              rpm.version,
                                              rpm.license,
                                              line.name,
                                              line.license))
            pkgs.append(rpm)
    return 0

if __name__ == "__main__":
    sys.exit(main())
