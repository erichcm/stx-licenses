#
# SPDX-License-Identifier: Apache-2.0
#


class LicenseException(Exception):
    pass

class RPMNotFound(LicenseException):
    pass

class RPMInvalidInfo(LicenseException):
    pass

class MissingStarlingXInfo(LicenseException):
    pass

class LibraryNotFound(LicenseException):
    pass

class LibraryPackageNotFound(LicenseException):
    pass

class RPMNotFoundInProviders(LicenseException):
    pass
