#
# SPDX-License-Identifier: Apache-2.0
#

import pytest
import utils

class TestStarlingXInfo:

    def test_is_startlingx_negative(self):
        stx = utils.StarlingXInfo()
        assert stx.is_starlingx() == False

    def test_is_startlingx(self):
        stx = utils.StarlingXInfo()
        stx.BUILD_INFO_FILE = "./build.info"
        assert stx.is_starlingx() == True

    def test_read_build_values(self):
        stx = utils.StarlingXInfo()
        stx.BUILD_INFO_FILE = "./build.info"

        stx.read_info()
        assert stx.os == "centos"
        assert stx.version == "19.01"
        assert stx.build_target == "Host Installer"
        assert stx.build_type == "Formal"
        assert stx.build_id == "20190515T220331Z"
        assert stx.job == "STX_build_master_master"
        assert stx.build_by == "starlingx.build@cengn.ca"
        assert stx.build_number == "102"
        assert stx.build_host == "starlingx_mirror"
        assert stx.build_date == "2019-05-15 22:03:31 +0000"

