#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name="stx-licenses",
    version=0.1,
    description="A tool to get licenses from StarlingX installation",
    author="Erich Cordoba",
    author_email="erich.cm@yandex.com",
    url="https://gitlab.com/erichcm/stx-licenses",
    packages=['stx_licenses'],
    license='Apache-2.0',
    scripts=['stx_licenses/stx-licenses.py']

)
