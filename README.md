# stx-licenses

![Build status](https://gitlab.com/erichcm/stx-licenses/badges/master/pipeline.svg)

A tool to get the licenses and dependencies of the StarlingX packages.

## Usage

```
cd stx_licenses
python stx-licenses.py
```

## Running tests

```
$ pip install -r test_requirements
$ pytest
```
